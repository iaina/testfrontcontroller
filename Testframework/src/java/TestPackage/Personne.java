/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestPackage;

import annotation.Urlannotation;
import model.ModelView;

/**
 *
 * @author Mofogasy
 */
public final class Personne {
    String idpersonne;
    String nom;
    String prenom;
    Integer age;
    String genre;
    
    public Personne() {
    }

    public Personne(String idpersonne, String nom, String prenom, Integer age, String genre) {
        this.idpersonne = idpersonne;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.genre = genre;
    }

    public void setIdpersonne(String idpersonne) {
        this.idpersonne = idpersonne;
    }

    public String getIdpersonne() {
        return idpersonne;
    }

    public Personne(String nom, String prenom, Integer age, String genre) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setAge(age);
        this.setGenre(genre);
    }
    @Urlannotation(url = "GetAll")
    public ModelView getAll(){   
        ModelView md = new ModelView();
        md.setUrl("ListeAll.jsp");
        Personne p = new Personne("HH", "HH", 12, "HH");
        md.setAttribute("Liste", p);
        return md;
    }
    @Urlannotation(url = "GetOne")
    public ModelView getOne(){
        ModelView md = new ModelView();
        md.setUrl("ListeAll.jsp");  
        Personne p = new Personne("HH", "HH", 12, "HH");
        md.setAttribute("Liste", p);
        return md;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Integer getAge() {
        return age;
    }

    public String getGenre() {
        return genre;
    }

}
